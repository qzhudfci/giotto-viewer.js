# giotto-viewer.js

giotto-viewer.js is a Javascript library that can visualize multi-modal spatial transcriptomic data.

It supports different view modes and allows users to interact between panels. 

It supports:

*  Physical view mode (cells arranged in their physical coordinates)
*  Expression view mode (cells arranged in dimensional reduction space such as tSNE, UMAP)
*  Morphology overlay
*  Staining overlay
*  Gene expression query
*  Multi-panel view, sync, and interaction
*  Cell selection function

## Demo

Mouse cortex: http://spatial.rc.fas.harvard.edu/giotto-demo/cortex/viewer.html

Olfactory bulb: http://spatial.rc.fas.harvard.edu/giotto-demo/OB/viewer.html


## Want to get started quickly?

Clone this repository.
```bash
hg clone https://bitbucket.org/qzhudfci/giotto-viewer.js
```

The spatial dataset should be first prepared (see tutorial how to do so). Download a prepared dataset ([mouse cortex: prepared.dataset.cortex.tar.gz](http://spatial.rc.fas.harvard.edu/giotto-viewer/prepared.dataset.cortex.tar.gz)).

Extract the content. And move the content to the current directory.
```bash
cd giotto-viewer.js
pwd
/home/qzhu/giotto/giotto-viewer.js
tar -zxf ~/Downloads/prepared.dataset.cortex.tar.gz
mv prepared.dataset.cortex/* .
```

Go to [**giotto-viewer-processing**](https://bitbucket.org/qzhudfci/giotto-viewer-processing/src) repository and download the files **`read_css_template.py`**, **`read_html_template.py`**, and **`read_config.py`**. Put these files in the current directory (`/home/qzhu/giotto/giotto-viewer.js`).

Download a setup JSON file which goes with the prepared dataset. [`setup.cortex.jun28.json`](https://bitbucket.org/qzhudfci/giotto-viewer.js/src/default/setup.cortex.jun28.json). The content of this file is below:

```json
{
"num_panel": 2,
"orientation": "horizontal", 
"annotation_set": {
	"num_annot": 2,
	"annot_1": {
		"file": "test.cell.type.unsupervised.id.txt",
		"name": "cell.type.unsup"
	},
	"annot_2": {
		"file": "test.hmrf.oct14.spatial.0.99.top100.b24.0.k9.cluster.txt",
		"name": "spatial.k9"
	}
},
"map_1": {
	"type": "PanelPhysical",
	"maxBound": 4096,
	"id": 1,
	"annot": "cell.type.unsup",
	"tile": "nissl",
	"dir_polyA": "imapr26.4",
	"dir_nissl": "imapr26.0",
	"dir_dapi": "imapr26.7",
	"gene_map": "10k.genes/gene.map",
	"segmentation_map": "segmentation.to.cell.centroid.map.txt",
	"segmentation": "roi.stitched.pos.all.cells.txt",
	"dir_gene_expression": "10k.genes",
	"gene_list": "gene.list.10k",
	"map_height": "500px"
},
"map_2": {
	"type": "PanelTsne",
	"maxBound": 500,
	"id": 2,
	"file_tsne": "test.map.tsne.coord.txt",
	"annot": "cell.type.unsup",
	"map_height": "500px"
},
"interact_1": ["map_1", "map_2"]
}
```
This file specifies where to find the datasets, how many panels, etc.
With this setup JSON file, run `read_config.py` to generate the corresponding HTML, CSS, and JS files for this dataset. The outputs are in the **-o**, **-p**, and **-q** flags.

```bash
python3 read_config.py
usage: read_config.py [-h] -c CONFIG -o OUTPUT_JS -p OUTPUT_HTML -q OUTPUT_CSS

python3 read_config.py -c setup.cortex.jun28.json -o test.jun28.js -p test.jun28.html -q test.jun28.css
```

Launch a web server locally.
```bash
python3 -m http.server
```

Go to the webserver and browse the generated html file.
```
http://localhost:8000/test.jun28.html
```

## Adapting giotto-viewer.js to your own dataset

First you need to prepare the dataset (see [tutorial here](http://spatial.rc.fas.harvard.edu/giotto-viewer/tutorial.html))

Then you need to write a JSON configuration file for configuring how you want the panels to be like.
(see samples: [2-panel](config/setup.cortex.2.json), [4-panel](config/setup.cortex.4.json), [6-panel](config/setup.cortex.6.json)). You can also configure the panel type in the JSON file, such as PanelTsne, PanelPhysical, and PanelPhysicalSimple.

Note at minimal, the dataset requires:

1. Cell physical coordinates
2. Cell UMAP/TSNE coordinates
3. Gene by cell expression matrix
4. Cluster annotations

With these information, you can setup the PanelTsne, PanelPhysicalSimple. However, PanelPhysical requires additional staining images and cell segmentation information.
